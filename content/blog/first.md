---
title: "Welcome to my blog. What is this place?"
date: 2019-01-28T00:31:20+05:30
draft: false
categories:
- Misc
Tags:
- misc
---

Although I did not grow up with the internet culture of the 90s and the early
00s, I occasionally find myself longing for "the good old days" where the
concerns about "Codes of Conduct", "Privacy" and "Censorship" were not as
prevalent as they are today.  Those were simpler times for the internet. Less
people. Less noise.  Less clutter.  Memes were neither weaponized nor forced.
'Teh internetz' was just another cool place to hang out and meet people rather
than... everything 

Granted, I am seeing this utopian vision of 'Teh Internetz' through the rose
tinted glasses of cultural nostalgia. Things were probably not as fun and cozy
as I "remember". Sure, I haven't forgotten some of the horrid things such as
slow internet, JRE plug-in, flash advertisements and an almost dysfunctional
video streaming.

However, if I were chosen the lord of the internet, I would do everything in my
power and bring about this utopian, decentralized and post social media era I
envision. Here, everyone with an online presence gets their own personal
website... to do whatever with. 

So, everything aside, I want to explain in this post, what this project (
personal website ) of mine aims to achieve, and why. The concept of a "personal
website" is alien to me, being from a generation that grew up with orkut and
facebook.

### 1. A place to write, express and articulate my thought

A personal website serves as a platform for anything I want to express. I know,
that there is social media for that sorta thing these days, however I see a
certain value in long form posts beyond the 280 characters limit and the
paradigm of "likes" and "shares". A blog post is just a piece of writing with
an independent existence. This makes blogs easier to parse as a reader.

Also, I've been wanting to get into the habit of writing regularly. Jordan
Peterson ji tells me that learning to articulate and verbalize my thoughts
through writing is a very useful skill to possess and sharpen.

{{< youtube id="bfDOoADCfkg" >}}

### 2. Online presence

This is a website where anyone can reach out and connect to me at any point of
time.

*"I can be useful to maintain an online presence"*, I have heard...  So yah.
This site is my online presence. I expect that this site improves my
*visibility*.  I don't do social media anyway. So it all works out.

While I do not agree with him on everything, this boomer gets it!

{{< youtube id="BYDwep9yI8A" >}}

### 3. This. Is. So. Cool.

THIS.👏 IS.👏 SO.👏 COOL.👏
 
I get a plot of land in the internet where I get to do anything I want.  It's
***MINE!*** 

What I am trying to say is...

THIS.👏 IS.👏 SO.👏 COOL.👏

Put another way, everyone should consider getting one of these.

# The future

I will write my opinion on everything from technology to politics here in the
blog. I do not see much else I can do here and am open to suggestions.
My next post will be me shouting at you to start using RSS feeds.

Thanks for the read!
