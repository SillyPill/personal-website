---
title: "I Made a Programming Language"
date: 2020-07-08T13:07:07+05:30
categories:
- programming
Tags:
- programming
- technology
- misc
---

Dear reader, I'm going to be honest with you. You just got pranked. EPIC STYLE!
*\*Dabs\* 😎*

The title is clickbait, but please do keep reading. You may find some
interesting things.

A *Domain Specific Language (DSL)* is a specialized language designed to solve a
very specific problem. Some examples might include
 
- **SQL,** designed only to query structured information from a relational
  database (primarily)
- **RegEx,** Regular expressions are used only for pattern matching, and nothing
  else.
- **HTML,** designed to represent a webpage mark-up.
- **Jinja,** used for web templating.

That is to say, the purpose of a DSL is to do one thing, and do it well.
\**Happy UNIX noises XD*\*

I had some code lying around in my computer that uses a greedy approach to
minimize cashflow in settling debts within a closed system. Think
[Splitwise](https://www.splitwise.com) or [Settle Up](https://settleup.io).  I
had been looking forward to dip my toes into meta-programming wizardry for a
while now, and so, I wrapped that around a parser (surprisingly easy to
implement) to create [setl.](https://gitlab.com/SillyPill/setl) 

### Setl :-

[Setl](https://gitlab.com/SillyPill/setl) is an [imperative](https://en.wikipedia.org/wiki/Imperative_programming)
DSL who's only purpose is to keep track of all the shared expenses in a closed
system, and calculate the "settlements".

This how you would add a shared expense in
[setl](https://gitlab.com/SillyPill/setl):

```txt
Foo: 100 @ Meal
```

OR

```txt
Foo:100, Bar:50 @ Meal
```

The idea is that users record all the expenses in a shared text file, using a
very simple, very intuitive syntax. A tool will later parse the text file and do
the calculating.

Refer to the [documentation](https://gitlab.com/SillyPill/setl) if you want to
learn more. 

In fewer than 500 lines of python code, [setl](https://gitlab.com/SillyPill/setl) can handle a
surprising number of edge cases. No hanky-panky apps, user authentication, etc.
Text STDIN and text STDOUT. The way gods intended it.

Very cool. 💪🏼❄️


This may possibly come in handy a few times if I survive the present apocalypse.

Now do I expect [setl](https://gitlab.com/SillyPill/setl) to gobble up Splitwise
and Settle Up's market share? Why yes ofcourse! Using bloat GUI "apps" is..
pfft.. **CRINGE!**

/s

However, here is the point I am trying to make. The ability to build DSL can
potentially be a very handy tool in a developer's toolkit. Especially for an
application that is not consumer facing. Also, a fairly simply DSL can solve a
fairly complex UI problem (provided it is designed well). If you want to learn
how to build a DSL, do not use [setl](https://gitlab.com/SillyPill/setl) as a
reference. That code reflects poorly on my skills as a code monkey.

There are 3 ways to implement a DSL.
- Abusing a host language features such as operator overloading and metaclasses
  to create an abomination (don't do this.) (or do, I'm not your dad)
- Implement a [finite state
  machine](https://en.wikipedia.org/wiki/Finite-state_machine.) And implement a
  line by line parser (using lots of regex.) This is the approach I used for
  [setl](https://gitlab.com/SillyPill/setl). It works fairly well. Especially for a language with simple grammar.
- Use a parser generator. (Traditionally, lex and yacc)

Allow me to direct you to some further reading.

[Further Reading.](https://www.youtube.com/watch?v=wQAPRfEOb10)

[More Further Reading.](https://martinfowler.com/books/dsl.html)

Have a nice day!
